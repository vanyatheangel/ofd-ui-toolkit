import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const CheckboxContainer = styled.div`
  display: inline-block;
  vertical-align: middle;
  position: relative;
`;

const Icon = styled.svg`
  fill: none;
  stroke: ${(props) => props.theme.black};
  stroke-width: 2px;
`;

const HiddenCheckbox = styled.input.attrs({ type: 'checkbox' })`
  border: 0;
  width: 17px;
  height: 17px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  top: 0;
  left: 0;
  opacity: 0;
  z-index: 1;
  white-space: nowrap;
  cursor: pointer;
  margin: 0;
`;

const StyledCheckbox = styled.div`
  display: inline-block;
  width: 15px;
  height: 15px;
  background: ${(props) => (props.checked ? 'linear-gradient(to top, #ffd919 98%, #ffb612 -39%)' : 'linear-gradient(to top, #e5e5e5, #ffffff)')};
  border: solid 1px rgba(0, 0, 0, 0.15);
  border-radius: 3px;
  transition: all 0.2s;

  ${Icon} {
    opacity: ${(props) => (props.checked ? 1 : 0)};
    transition: opacity 0.2s;
  }
`;

const Checkbox = ({
  checked,
  onChange,
  ...props
}) => (
  <CheckboxContainer>
    <HiddenCheckbox checked={checked} onChange={onChange} {...props} />
    <StyledCheckbox checked={checked}>
      <Icon viewBox="0 0 24 24">
        <polyline points="20 6 9 17 4 12" />
      </Icon>
    </StyledCheckbox>
  </CheckboxContainer>
);

Checkbox.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func,
};

Checkbox.defaultProps = {
  checked: false,
  onChange: () => null,
};

export default Checkbox;
