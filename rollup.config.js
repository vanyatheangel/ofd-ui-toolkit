import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import external from 'rollup-plugin-peer-deps-external';
import resolve from '@rollup/plugin-node-resolve';
import url from '@rollup/plugin-url';
import svgr from '@svgr/rollup';

import pkg from './package.json';

export default {
  input: 'src/index.js',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      sourcemap: true,
    },
    {
      file: pkg.module,
      format: 'es',
      sourcemap: true,
    },
  ],
  external: [
    'styled-components',
    'formik',
  ],
  plugins: [
    external(),
    url({
      include: ['**/*.woff', '**/*.woff2', '**/*.svg'],
      limit: Infinity,
    }),
    svgr(),
    babel({
      exclude: 'node_modules/**',
    }),
    resolve({
      extensions: [ '.js', '.jsx' ],
      browser: true
    }),
    commonjs({
      include: 'node_modules/**',
      namedExports: {
        'node_modules/react-is/index.js': [ 'isFragment', 'ForwardRef', 'typeOf', ' isElement', 'isValidElementType' ],
        'node_modules/react-dom/index.js': [ 'createPortal', 'findDOMNode' ],
        'node_modules/react-datepicker/dist/react-datepicker.min.js': ['registerLocale'],
      },
    }),
  ],
};
