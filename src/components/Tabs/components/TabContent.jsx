import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const TabBlock = styled.div`
  display: block;
  width: 100%;
`;

export const TabContent = ({ children }) => (
  <TabBlock>
    {children}
  </TabBlock>
);

TabContent.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default TabContent;
