import styled from 'styled-components';

const Wrapper = styled.section`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding-top: 30px;
  box-sizing: border-box;
`;

export default Wrapper;
