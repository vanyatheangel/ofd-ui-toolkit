import React, { useEffect } from 'react';
import styled from 'styled-components';
import Button from '../Button';
import Popup from '../Popup';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const ButtonsWrapper = styled.div`
  display: flex;
`;

const SubmitPopup = ({ title, isOpen, onClose, handleSubmit, disabled, data, loading, error }) => {
  useEffect(() => {
    if (data && !loading && !error) {
      onClose();
    }
  }, [ data, loading, error ]);

  return (
    <Popup isOpen={isOpen} onClose={onClose}>
      <Wrapper>
        <h1>{title}</h1>
        <ButtonsWrapper>
          <Button
            color="white"
            onClick={onClose}
            extendStyle="margin-right: 30px;"
            large
          >
            Нет
          </Button>
          <Button
            onClick={handleSubmit}
            disabled={disabled}
            large
          >
            Да
          </Button>
        </ButtonsWrapper>
      </Wrapper>
    </Popup>
  );
}

export default SubmitPopup;
