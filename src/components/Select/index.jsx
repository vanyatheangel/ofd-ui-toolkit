import React, { useEffect } from 'react';
import ReactSelect from 'react-select';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useField } from 'formik';

const Title = styled.span`
  font-size: 16px;
  line-height: 1.47;
  color: ${(props) => props.color || props.theme.gray};
  margin-bottom: 10px;
`;

const styles = {
  container: (base, props) => ({
    ...base,
    width: props.theme.white ? '100%' : '200px',
    marginBottom: props.theme.white ? '30px' : '0',
  }),
  menu: (base, props) => ({
    ...base,
    width: props.theme.white ? '100%' : '350px',
    backgroundColor: props.theme.white ? '#ffffff' : '#ececec',
    borderRadius: '2px',
    boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.24)',
  }),
  indicatorSeparator: () => ({
    display: 'none',
  }),
  indicatorsContainer: (base) => ({
    padding: 0,
  }),
  control: (base, props) => ({
    display: 'flex',
    alignItems: 'center',
    marginRight: props.theme.white ? '0' : '20px',
    height: '44px',
    borderRadius: props.theme.white ? '3px' : '4px',
    backgroundColor: props.theme.white ? '#ffffff' : '#ececec',
    boxShadow: props.theme.white ? 'inset 0 3px 0 0 rgba(0,0,0,0.13)' : 'none',
    border: props.theme.white ? 'solid 1px rgba(170,170,170,0.45)' : 'none',
  }),
  placeholder: (base) => ({
    ...base,
    color: '#282828',
  }),
  option: (base) => ({
    ...base,
    // backgroundColor: '#787878',
    ':hover': {
      backgroundColor: '#d4d4d4',
    },
    ':active': {
      backgroundColor: '#d4d4d4',
    },
    ':focus': {
      backgroundColor: '#d4d4d4',
    },
  }),
};

const Select = ({
  options = [], onChange, value, defaultSelectValue, formik, white, name, disabled, customOnChange, readOnly, onInputChange, isSearchable, id = '', selectPlaceholder
}) => {
  const transformedOptions = options && defaultSelectValue
    ? [ { label: defaultSelectValue, value: null }, ...options ]
    : options;

  const commonProps = {
    isDisabled: disabled,
    styles,
    placeholder: defaultSelectValue || selectPlaceholder,
    isSearchable,
    theme: (theme) => ({
      ...theme,
      white: formik || white,
      colors: {
        ...theme.colors,
        primary25: '#d4d4d4',
        primary50: 'black',
      },
    }),
    id,
  };

  const handleFormikChange = (onChangeFunc, values) => {
    if (customOnChange) {
      customOnChange(values);
    }
    return onChangeFunc;
  };

  if (formik) {
    const [ field, _, helpers ] = useField(name);

    useEffect(() => {
      if (disabled && value) {
        helpers.setValue(value);
      }
    }, []);


    if (readOnly) {
      const valueToDisplay = value || (options?.length
        ? options.find((option) => option?.value === field?.value)?.label || ''
        : '');

      return (
        <Title color="black">
          {valueToDisplay}
        </Title>
      );
    }

    const handleInputChange = (newValue) => {
      onInputChange(newValue);
    };

    return (
      <ReactSelect
        options={Array.isArray(transformedOptions) && transformedOptions?.length > 0
          ? transformedOptions
          : []}
        name={field.name}
        onBlur={field.onBlur}
        onChange={(option) => handleFormikChange(helpers.setValue(option.value), option.value)}
        onInputChange={handleInputChange}
        isSearchable={isSearchable}
        value={
          // eslint-disable-next-line no-nested-ternary
          disabled && value
            ? value
            : options && Array.isArray(options)
              ? options.find((option) => option.value === field.value)
              : ''
        }
        filterOption={() => true} // disable react-select internal filter function, since we are filtering using backend
        placeholder={selectPlaceholder}
        {...commonProps}
      />
    );
  }

  if (readOnly) {
    return (
      <Title color="black">
        {value || ''}
      </Title>
    );
  }

  return (
    <ReactSelect
      name={name}
      options={transformedOptions}
      onChange={onChange}
      value={value}
      placeholder={selectPlaceholder}
      {...commonProps}
    />
  );
};

Select.propTypes = {
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
    label: PropTypes.string.isRequired,
  })),
  onChange: PropTypes.func,
  value: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
    label: PropTypes.string.isRequired,
  })),
  formik: PropTypes.bool,
  selectPlaceholder: PropTypes.string,
  white: PropTypes.bool,
  onInputChange: PropTypes.func,
  isSearchable: PropTypes.bool,
};

Select.defaultProps = {
  value: null,
  formik: false,
  selectPlaceholder: 'Выберите...',
  onChange: () => {},
  white: false,
  onInputChange: () => {},
  isSearchable: false,
  options: [],
};

export default Select;
