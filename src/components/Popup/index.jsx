import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import cross from '../../assets/images/cross.svg';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
  padding: 6px;
  position: fixed;
  top: 0;
  left: 0;
  overflow: auto;
  text-align: center;
  z-index: ${(props) => (props.isOpen ? 10 : -10)};
  background: rgba(193, 190, 185, 0.7);
  transition: background 0.5s, transform 0.5s, opacity 0.1s;
  opacity: ${(props) => (props.isOpen ? 1 : 0)};
  cursor: pointer;
  ${(props) => props.wrapperStyle}
`;

const Body = styled.div`
  display: inline-block;
  padding: 110px 140px 60px;
  z-index: 11;
  border-radius: 4px;
  background: #fff;
  position: relative;
  cursor: default;
  ${(props) => props.bodyStyle}
`;

const CrossButton = styled.button`
  position: absolute;
  top: 30px;
  right: 34px;
  background: none;
  border: none;
  cursor: pointer;
`;

export const Popup = ({
  children,
  isOpen,
  onClose,
  bodyStyle,
  wrapperStyle,
  id = '',
}) => {
  const wrapperRef = useRef(null);

  const handleWrapperClick = (event) => {
    if (onClose && event.target === wrapperRef.current) {
      onClose();
    }
  };

  // to prevent double scrolling, while popup is open
  useEffect(() => {
    if (isOpen) {
      document.body.style.overflowY = 'hidden';
    } else {
      document.body.style.overflowY = 'auto';
    }
  }, [isOpen]);

  return (
    <Wrapper id={id} isOpen={isOpen} onClick={handleWrapperClick} ref={wrapperRef} wrapperStyle={wrapperStyle}>
      <Body bodyStyle={bodyStyle}>
        <CrossButton onClick={onClose}>
          <img src={cross} alt="close" />
        </CrossButton>
        {children}
      </Body>
    </Wrapper>
  );
};

Popup.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.string,
  ]).isRequired,
  isOpen: PropTypes.bool,
  onClose: PropTypes.func,
  bodyStyle: PropTypes.string,
  wrapperStyle: PropTypes.string,
};

Popup.defaultProps = {
  isOpen: false,
  onClose: () => null,
  bodyStyle: '',
  wrapperStyle: '',
};

export default Popup;
