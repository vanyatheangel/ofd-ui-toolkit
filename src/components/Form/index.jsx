import React, { useMemo } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Input from '../Input';
import Select from '../Select';

const InputsList = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  max-width: 900px;
`;

const InputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 370px;
  margin-right: 40px;
`;

const Title = styled.span`
  font-size: 16px;
  line-height: 1.47;
  color: ${(props) => props.color || props.theme.gray};
  margin-bottom: 10px;
`;

const Form = (props) => {
  const {
    values,
    touched,
    errors,
    dirty,
    isSubmitting,
    handleChange,
    handleBlur,
    handleReset,
    inputsList,
    data,
    readOnly,
    dicts,
    customOnInputChange,
    customOnSelectChange,
    exceptionAfterFilters,
    exceptionForKeys,
    onSelectInputChange = () => {},
  } = props;

  const handleInputChange = (e) =>
    // if (customOnInputChange) {
    //   customOnInputChange(values);
    // }
    handleChange(e);
  const getExceptions = (key) => {
    if (exceptionAfterFilters && exceptionForKeys.includes(key)) {
      return exceptionAfterFilters;
    }
  };

  return (
    <InputsList>
      {inputsList.map(({
        title,
        key,
        textArea,
        disabled,
        type = 'text',
        select,
        dict,
        defaultSelectLabel,
        defaultSelectValue,
        afterFilters,
        isSearchable = false,
        selectValueKey = 'id',
        selectLabelKey = 'name',
      }) => {
        const currentDict = dicts?.[key];
        let defaultValue = data?.[key];

        if (dict) {
          const result = currentDict?.find(({ id }) => id === data?.[key]) || {};
          defaultValue = result[selectLabelKey];
        }

        if (select) {
          const options = currentDict?.length && currentDict?.map((el) => ({
            value: el[selectValueKey],
            label: el[selectLabelKey],
          }));

          return (
            <InputWrapper key={title}>
              <Title>{title}</Title>
              <Select
                disabled={disabled || readOnly}
                readOnly={readOnly}
                key={title}
                name={key}
                options={options}
                defaultSelectValue={defaultSelectLabel}
                isSearchable={isSearchable}
                customOnChange={customOnSelectChange}
                onInputChange={(e) => onSelectInputChange(key, e)}
                value={defaultSelectValue}
                formik
              />
            </InputWrapper>
          );
        }

        return (
          <InputWrapper key={title}>
            <Input
              name={key}
              key={title}
              title={title}
              defaultValue={defaultValue}
              textArea={textArea}
              type={type}
              datepickerClassname={type === 'date' ? 'reactdatepicker-input' : ''}
              disabled={disabled}
              value={values?.[key]}
              onChange={(e) => handleInputChange(e)}
              onBlur={handleBlur}
              readOnly={readOnly}
              afterFilters={afterFilters}
              exceptionAfterFilters={exceptionForKeys ? getExceptions(key) : null}
              error={touched[key] && errors[key]}
            />
          </InputWrapper>
        );
      })}
    </InputsList>
  );
};

Form.propTypes = {
  readOnly: PropTypes.bool,
  data: PropTypes.shape({}).isRequired,
  inputsList: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
    textArea: PropTypes.bool,
    disabled: PropTypes.bool,
    type: PropTypes.string,
  })).isRequired,
};

Form.defaultProps = {
  readOnly: false,
};

export default Form;
