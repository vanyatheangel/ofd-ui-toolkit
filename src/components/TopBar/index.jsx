import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Button from '../Button';
import Arrow from '../Arrow';
import arrowIcon from '../../assets/images/arrow-icon.svg';

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  //justify-content: space-between;
  //align-items: center;
  margin-bottom: 35px;
`;

const Heading = styled.span`
  font-size: 30px;
  line-height: 1.4;
  font-weight: normal;
`;

const TopBar = ({ title, buttonTitle, onClick, onArrowIconClick }) => (
  <Wrapper>
    <div style={{ display: 'flex' }}>
      {
        onArrowIconClick && (
          <Arrow onClick={onArrowIconClick}>
            <img src={arrowIcon} alt="" />
          </Arrow>
        )
      }
      <Heading>
        {title}
      </Heading>
    </div>
    {buttonTitle && (
      <Button
        onClick={onClick}
        extendStyle="width: 231px;"
        extendWrapperStyle="display: flex; justify-content: flex-end;"
      >
        {buttonTitle}
      </Button>
    )}
  </Wrapper>
);

TopBar.propTypes = {
  title: PropTypes.string.isRequired,
  buttonTitle: PropTypes.string,
  onClick: PropTypes.func,
};

TopBar.defaultProps = {
  buttonTitle: null,
  onClick: () => {},
};

export default TopBar;
