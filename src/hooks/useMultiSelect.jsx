import { useEffect, useState } from 'react';

const useMultiSelect = (data) => {
  const [ idList, setIdList ] = useState({});
  const [ allCheckbox, setAllCheckbox ] = useState(false);

  useEffect(() => {
    const ids = data.map(({ id }) => id);
    const newState = ids.reduce((acc, curr) => {
      acc[curr] = allCheckbox;
      return acc;
    }, {});

    setIdList(newState);
  }, [allCheckbox]);

  const changeAll = () => setAllCheckbox(!allCheckbox);
  const changeSingle = (id) => {
    setIdList({
      ...idList,
      [id]: idList[id] != null
        ? !idList[id]
        : true,
    });
  };

  return {
    changeAll,
    changeSingle,
    allCheckbox,
    idList,
    setIdList,
  };
};

export default useMultiSelect;
