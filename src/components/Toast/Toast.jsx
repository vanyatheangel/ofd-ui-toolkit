import React, { useEffect, useRef } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { theme } from '../../../lib/styled';

const StyledToast = styled.div`
  border: 2px solid transparent;
  background-color: ${(props) => (
    // eslint-disable-next-line no-nested-ternary
    props.toastType === 'error'
      ? props.theme.pink
      : props.toastType === 'success'
        ? props.theme.green
        : props.theme.lightGray
  )};
  border-radius: 4px;
  max-width: 480px;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, .2);
  margin-top: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  cursor: pointer;
`;

const ToastText = styled.div`
  padding: 16px 0;
  margin-right: 24px;
  line-height: 1.4;
  font-size: 15px;
  line-height: 1.27;
  color: ${(props) => (props.toastType === 'error' ? props.theme.errorRed : props.theme.dark)};
`;

const ErrorBlock = styled.div`
  display: flex;
  flex-direction: column;
  color: ${(props) => props.theme.errorRed};
`;

const HandIcon = styled.img`
  padding: 9px 18px;
`;

const StyledToastComponent = ({ children, remove, toastType }) => {
  const removeRef = useRef();
  removeRef.current = remove;

  const errorText = children && toastType === 'error'
    ? (Array.isArray(children) || typeof children === 'string') && children
    : 'Непредвиденная ошибка';

  const contentToRender = toastType === 'error'
    ? (
      <ErrorBlock>
        {errorText}
      </ErrorBlock>
    )
    : children;

  useEffect(() => {
    const duration = 5000;
    const id = setTimeout(() => removeRef.current(), duration);

    return () => clearTimeout(id);
  }, []);

  return (
    <StyledToast onClick={remove} toastType={toastType} className="toast">
      <HandIcon src="../../../assets/images/hand.png" />
      <ToastText toastType={toastType}>
        {contentToRender}
      </ToastText>
    </StyledToast>
  );
};

const Toast = (props) => (
  <ThemeProvider theme={theme}>
    <StyledToastComponent {...props} />
  </ThemeProvider>
);

export default Toast;
