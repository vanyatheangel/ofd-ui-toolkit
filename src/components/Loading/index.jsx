import React from 'react';
import PropTypes from 'prop-types';
import Loader from '../Loader';

const Loading = ({ loadingList, children, size, actualHeight }) => {
  if (Array.isArray(loadingList) && loadingList.includes(true)) {
    return <Loader size={size} centered actualHeight={actualHeight} />;
  }

  return children;
};

Loading.propTypes = {
  loadingList: PropTypes.arrayOf(PropTypes.bool),
  size: PropTypes.oneOf([ 'default', 'big', 'small' ]),
};

Loading.defaultProps = {
  size: 'default',
  loadingList: [],
};

export default Loading;
