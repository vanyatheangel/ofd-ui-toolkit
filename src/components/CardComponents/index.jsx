import styled from 'styled-components';

const CardHeaderWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const TitleHeader = styled.h1`
  font-size: 30px;
  line-height: 1.4;
  color: ${(props) => props.theme.dark};
  font-weight: normal;
  margin: 0;
`;

const NameHeader = styled.div`
  display: inline-flex;
  margin: 20px 0 40px;
`;

const NameHeaderText = styled.h2`
  font-size: 21px;
  line-height: 1.38;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  color: ${(props) => props.theme.dark};
  margin-left: 12px;
`;

const InputsList = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  max-width: 900px;
`;

const CheckboxesList = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 25px;
  margin-top: 25px;
`;

const CheckboxLine = styled.div`
  display: flex;
  margin-bottom: 18px;
  &:last-child {
  margin-bottom: 0;
  }
`;

const CheckboxLabel = styled.label`
  display: block;
  margin-left: 11px;
  font-size: 17px;
  line-height: 1.47;
  cursor: pointer;
`;

const ChildrenWrapper = styled.label`
  display: flex;
  justify-content: flex-end;
`;

export {
  CardHeaderWrapper,
  TitleHeader,
  NameHeader,
  NameHeaderText,
  InputsList,
  CheckboxesList,
  CheckboxLine,
  CheckboxLabel,
  ChildrenWrapper,
};
