import React from 'react';
import PropTypes from 'prop-types';

const TableDictCell = ({ dict, field }) => {
  if (dict?.length > 0) {
    const result = dict.find(({ id }) => id === field) || {};
    const { name = null } = result;
    return name;
  }
  return null;
};

TableDictCell.propTypes = {
  dict: PropTypes.arrayOf({
    id: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  field: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]).isRequired,
};

export default TableDictCell;
