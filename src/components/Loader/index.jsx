import React from 'react';
import PropTypes from 'prop-types';
import styled, { keyframes } from 'styled-components';

const spinner = keyframes`
  to {
    transform: rotate(360deg);
  }
`;

const StyledLoader = styled.div`
  width: ${(props) => (
    // eslint-disable-next-line no-nested-ternary
    props.size === 'big'
      ? '130px'
      : props.size === 'small'
        ? '70px'
        : '100px'
  )};
  height: ${(props) => (
    // eslint-disable-next-line no-nested-ternary
    props.size === 'big'
      ? '130px'
      : props.size === 'small'
        ? '70px'
        : '100px'
  )};
  background-color: transparent;
  border-radius: 50%;
  border: 10px solid ${(props) => props.theme.yellow};
  border-top-color: transparent;
  animation: ${spinner} 1100ms ease infinite;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: ${(props) => props.actualHeight || 'auto'};
`;

const Loader = ({
  size,
  centered,
  actualHeight,
}) => {
  if (centered) {
    return (
      <Wrapper actualHeight={actualHeight}>
        <StyledLoader size={size} />
      </Wrapper>
    );
  }

  return (
    <StyledLoader size={size} />
  );
};

Loader.propTypes = {
  size: PropTypes.oneOf([ 'default', 'big', 'small' ]),
  centered: PropTypes.bool,
};

Loader.defaultProps = {
  size: 'default',
  centered: false,
};

export default Loader;
