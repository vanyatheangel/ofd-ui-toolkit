import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const TabBlock = styled.span`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 125px;
  height: 49px;
  border: solid 2px ${(props) => props.theme.lightGray};
  border-left: none;
  border-right: none;
  margin-top: 4px;
  color: ${(props) => props.theme.gray};
  cursor: pointer;
  white-space: nowrap;
  padding: 0 4px;

  &:first-child {
    border-left: solid 2px ${(props) => props.theme.lightGray};
    border-bottom-left-radius: 3px;
    border-top-left-radius: 3px;

    &.active {
      margin-left: 2px;
    }
  }

  &:last-child {
    border-right: solid 2px ${(props) => props.theme.lightGray};
    border-bottom-right-radius: 3px;
    border-top-right-radius: 3px;
  }

  &.active {
    padding: 4px;
    border: solid 2px ${(props) => props.theme.lightOrange};
    border-radius: 3px;
    margin-top: 0;
    color: ${(props) => props.theme.black};
  }
`;

export const Tab = ({
  children,
  active,
  onClick,
  tabIndex,
}) => (
  <TabBlock onClick={() => onClick(tabIndex)} className={active ? 'active' : null}>
    {children}
  </TabBlock>
);

Tab.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
    PropTypes.element,
  ]).isRequired,
  tabIndex: PropTypes.number.isRequired,
  active: PropTypes.bool,
  onClick: PropTypes.func,
};

Tab.defaultProps = {
  active: false,
  onClick: () => null,
};

export default Tab;
