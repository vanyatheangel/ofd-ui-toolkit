import React from 'react';
import PropTypes from 'prop-types';
import { NameHeader, NameHeaderText, TitleHeader, CardHeaderWrapper, ChildrenWrapper } from '../CardComponents';
import Arrow from '../Arrow';
import arrowIcon from '../../assets/images/arrow-icon.svg';

const CardHeader = ({ arrowClick, header, name, children }) => (
  <>
    <CardHeaderWrapper>
      <TitleHeader>{header}</TitleHeader>
    </CardHeaderWrapper>
    <ChildrenWrapper>
      {children}
    </ChildrenWrapper>
    <NameHeader>
      {
        arrowClick ? (
          <Arrow onClick={arrowClick}>
            <img src={arrowIcon} alt="" />
          </Arrow>
        ) : null
      }
      <NameHeaderText>{name}</NameHeaderText>
    </NameHeader>
  </>
);

CardHeader.propTypes = {
  arrowClick: PropTypes.func.isRequired,
  header: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  name: PropTypes.string,
};

CardHeader.defaultProps = {
  name: null,
};

export default CardHeader;
