# ofduitoolkit

> ofd ui toolkit

[![NPM](https://img.shields.io/npm/v/ofduitoolkit.svg)](https://www.npmjs.com/package/ofduitoolkit) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save ofduitoolkit
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'ofduitoolkit'
import 'ofduitoolkit/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [OFD](https://github.com/OFD)
