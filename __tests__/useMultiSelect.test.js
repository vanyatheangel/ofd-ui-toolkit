// import { renderHook, act } from '@testing-library/react-hooks';
// import { useMultiSelect } from '@ofd-ui-toolkit';
//
// const data = [
//   {
//     id: 1,
//   },
//   {
//     id: 2,
//   },
//   {
//     id: 3,
//   },
// ];
//
// describe('changeAll function', () => {
//   test('should select all checkboxes', () => {
//     const {
//       result,
//     } = renderHook(() => useMultiSelect(data));
//
//     act(() => {
//       result.current.changeAll();
//     });
//     expect(result.current.idList).toMatchSnapshot();
//     expect(result.current.allCheckbox).toBe(true);
//   });
//
//   test('should select and deselect all checkboxes', () => {
//     const {
//       result,
//     } = renderHook(() => useMultiSelect(data));
//
//     act(() => {
//       result.current.changeAll();
//     });
//     act(() => {
//       result.current.changeAll();
//     });
//     expect(result.current.idList).toMatchSnapshot();
//     expect(result.current.allCheckbox).toBe(false);
//   });
// });
//
// describe('changeSingle function', () => {
//   test('should change id=1 back and forth', () => {
//     const {
//       result,
//     } = renderHook(() => useMultiSelect(data));
//
//     act(() => {
//       result.current.changeSingle(1);
//     });
//     expect(result.current.idList).toMatchSnapshot();
//     expect(result.current.idList['1']).toBe(true);
//
//     act(() => {
//       result.current.changeSingle(1);
//     });
//     expect(result.current.idList['1']).toBe(false);
//   });
// });
