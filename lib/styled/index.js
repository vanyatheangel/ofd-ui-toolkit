import { createGlobalStyle } from 'styled-components';

import OfficinaSerifBookC from '../../src/assets/fonts/OfficinaSerifBookC.woff';
import OfficinaSerifBoldC from '../../src/assets/fonts/OfficinaSerifBoldC.woff'

export const theme = {
  black: '#000',
  white: '#fff',
  dark: '#282828',
  roseDust: '#eeeae5',
  yellow: '#ffdc7d',
  paleYellow: '#eed99f',
  lightOrange: '#fac12b',
  gray: '#787878',
  lightGray: '#e9e9e9',
  baseButtonHeight: '52px',
  green: '#80b980',
  red: '#e44141',
  errorRed: '#dc0000',
  pink: '#fff0f0',
};

export const GlobalStyles = createGlobalStyle`
@font-face {
  font-family: 'Officina Serif';
  src: url('${OfficinaSerifBoldC}') format('woff');
  font-weight: bold;
  font-style: normal;
}

@font-face {
  font-family: 'Officina Serif';
  src: url('${OfficinaSerifBookC}') format('woff');
  font-weight: normal;
  font-style: normal;
}

body {
  margin: 0;
  padding: 0;
  font-family: 'Officina Serif', sans-serif;
}
`;

export default theme;
