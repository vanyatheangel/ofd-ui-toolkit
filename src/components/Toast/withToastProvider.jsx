import React, { useState, useMemo } from 'react';
import { createPortal } from 'react-dom';
import styled from 'styled-components';

import ToastContext from './context';
import Toast from './Toast';

const ToastWrapper = styled.div`
  position: fixed;
  bottom: 20px;
  right: 20px;
  z-index: 11;
`;

// Create a random ID
const generateUEID = () => {
  let first = (Math.random() * 46656) | 0;
  let second = (Math.random() * 46656) | 0;
  first = (`000${first.toString(36)}`).slice(-3);
  second = (`000${second.toString(36)}`).slice(-3);

  return first + second;
};

const withToastProvider = (Component) => {
  const WithToastProvider = (props) => {
    const [ toasts, setToasts ] = useState([]);
    const add = ({ type, content }) => {
      const id = generateUEID();

      setToasts([ ...toasts, { id, content, type } ]);
    };
    const remove = (id) => setToasts(toasts.filter((toast) => toast.id !== id));
    const providerValue = useMemo(() => ({ add, remove }), [toasts]);

    return (
      <ToastContext.Provider value={providerValue}>
        <Component {...props} />

        {createPortal(
          <ToastWrapper>
            {toasts.map((toast) => (
              <Toast key={toast.id} remove={() => remove(toast.id)} toastType={toast.type}>
                {toast.content}
              </Toast>
            ))}
          </ToastWrapper>,
          document.body,
        )}
      </ToastContext.Provider>
    );
  };

  return WithToastProvider;
};

export default withToastProvider;
