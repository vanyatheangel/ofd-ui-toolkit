import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Tab from './Tab';

const TabsList = styled.div`
  display: flex;
  flex-direction: row;
`;

const TabPanel = styled.div`
  display: block;
  margin-top: 25px;
  padding-left: 4px;
`;

const Tabs = ({
  selectedIndex,
  onSelect,
  children,
}) => (
  <div>
    <TabsList>
      {children.filter((el) => el !== null).map((el, i) => {
        const { label } = el.props;

        return (
          <Tab
            active={selectedIndex === i}
            tabIndex={i}
            key={label}
            onClick={onSelect}
          >
            {label}
          </Tab>
        );
      })}
    </TabsList>
    <TabPanel>
      {children.filter((el, i) => selectedIndex === i)}
    </TabPanel>
  </div>
);

Tabs.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
  onSelect: PropTypes.func,
  selectedIndex: PropTypes.number,
};

Tabs.defaultProps = {
  onSelect: () => null,
  selectedIndex: 0,
};

export default Tabs;
