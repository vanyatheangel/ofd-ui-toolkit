// import { renderHook, act } from '@testing-library/react-hooks';
// import { useAction, kktActions } from '@ofd-ui-toolkit';
// import endpointsList from '../src/app/api';
//
// test('lastAction and currentAction', () => {
//   const {
//     result,
//   } = renderHook(() => useAction({ actionDict: kktActions, endpointsList }));
//   const action = kktActions[1]?.text;
//   act(() => {
//     result.current.setLastAction(action);
//   });
//
//   expect(result.current.lastAction).toBe(action);
//   expect(result.current.currentAction).toMatchSnapshot();
//   expect(result.current.currentAction.popupText({ singleAction: true })).toMatchSnapshot();
//   expect(result.current.currentAction.popupText({ singleAction: false })).toMatchSnapshot();
// });
//
// test('single id=1', () => {
//   const {
//     result,
//   } = renderHook(() => useAction({ actionDict: kktActions, endpointsList }));
//   act(() => {
//     result.current.setSingleId(1);
//   });
//
//   expect(result.current.singleId).toBe(1);
// });
