import React from 'react';
import PropTypes from 'prop-types';
import styled, { css } from 'styled-components';

import Button from '../Button';
import Loader from '../Loader';

const extendStyle = css`
  background: none;
  border-bottom: dotted 2px ${(props) => props.theme.black};
  padding: 0;
  margin: 0;
  border-radius: 0;
  height: 26px;

  &:not(:disabled) {
    &:active {
      box-shadow: none;
    }
  }
`;

const Wrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ShowMoreButton = ({
  text,
  isLoading,
  isHidden,
  onClick,
}) => (
  isHidden
    ? null
    : (
      <Wrapper>
        {
          isLoading
            ? (
              <Loader />
            )
            : (
              <Button extendStyle={extendStyle} onClick={onClick}>
                {text}
              </Button>
            )
        }
      </Wrapper>
    )
);

ShowMoreButton.propTypes = {
  text: PropTypes.string,
  isLoading: PropTypes.bool,
  isHidden: PropTypes.bool,
  onClick: PropTypes.func,
};

ShowMoreButton.defaultProps = {
  text: 'Показать еще',
  isLoading: false,
  isHidden: false,
  onClick: () => null,
};

export default ShowMoreButton;
