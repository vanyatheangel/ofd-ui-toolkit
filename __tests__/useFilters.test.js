// import { renderHook, act } from '@testing-library/react-hooks';
// import useFilters, { SORT_ASC, SORT_DESC } from '@ofd-ui-toolkit';
//
// const data = [
//   {
//     field: 'astatus',
//     value: 'A',
//   },
//   {
//     field: 'name',
//     value: 'test',
//     type: 'contains',
//   },
// ];
//
// test('filters', () => {
//   const {
//     result,
//   } = renderHook(() => useFilters(data));
//
//   expect(result.current.filters).toMatchSnapshot();
// });
//
// test('change sort field', () => {
//   const {
//     result,
//   } = renderHook(() => useFilters(data));
//
//   act(() => {
//     result.current.changeSortField('test');
//   });
//
//   expect(result.current.filters).toMatchSnapshot();
//   expect(result.current.sortDirection).toBe(SORT_ASC);
//   expect(result.current.sortField).toBe('test');
// })
//
// test('change sort field twice', () => {
//   const {
//     result,
//   } = renderHook(() => useFilters(data));
//
//   act(() => {
//     result.current.changeSortField('test');
//   });
//
//   act(() => {
//     result.current.changeSortField('test');
//   });
//
//   expect(result.current.filters).toMatchSnapshot();
//   expect(result.current.sortDirection).toBe(SORT_DESC);
//   expect(result.current.sortField).toBe('test');
// });
