import styled from 'styled-components';
import PropTypes from 'prop-types';

const Circle = styled.div`
  min-width: ${props => props.size || '12px'};
  min-height: ${props => props.size || '12px'};
  border-radius: 50%;
  display: inline-block;
  background-color: ${({ theme, color }) => theme[color]};
  ${(props) => props.extendStyle}
`;

Circle.propTypes = {
  color: PropTypes.oneOf('green', 'red', 'gray'),
};

Circle.defaultProps = {
  color: 'gray',
};

export default Circle;
