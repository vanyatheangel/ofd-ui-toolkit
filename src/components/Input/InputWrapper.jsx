import styled from 'styled-components';

export const isWidthSmall = (small) => (small ? '275px' : '370px');

export const InputWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: ${({ width, small }) => width || isWidthSmall(small)};
  margin-right: ${({ noMarginRight }) => noMarginRight ? 0 : '40px'};
  align-items: ${({ alignItems }) => alignItems || 'stretch'};
  
  & * {
    font-family: 'Officina Serif', sans-serif;
  }
`;
