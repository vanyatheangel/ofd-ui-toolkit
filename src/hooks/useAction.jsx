import { useState } from 'react';
import useAPI from './useAPI';

const useAction = ({ actionDict, endpointsList }) => {
  const [ lastAction, setLastAction ] = useState(null);
  const [ singleId, setSingleId ] = useState(null);
  const [ allItemData, setAllItemData ] = useState(null);

  const currentAction = actionDict?.find(({ text }) => text === lastAction);

  const getApiConfig = () => (currentAction
    ? endpointsList[currentAction?.key]?.()
    : {});

  const {
    data: actionData, loading, error, fetch,
  } = useAPI(getApiConfig(), false);

  const submit = async (data) => {
    // tplan, is_reneval, astatus, next_tplan_date, isTplanReneval
    const params = {
      data,
    };

    await fetch(params);
  };

  return {
    lastAction,
    setLastAction,
    currentAction,
    singleId,
    setSingleId,
    allItemData,
    setAllItemData,
    data: actionData,
    loading,
    error,
    submit,
  };
};

const lessorKktActions = [
  {
    text: 'Закрыть доступ',
    key: 'closeKktAccess',
    popupText: () => 'Вы действительно хотите закрыть доступ к текущей кассе?',
  },
];

const kktActions = [
  {
    text: 'Открыть доступ',
    key: 'openKktAccess',
    popupText: () => 'Выберите Арендодателя',
  },
  {
    text: 'Активировать',
    key: 'activateKkt',
    popupText: ({ singleAction }) => (singleAction
      ? 'Вы действительно хотите активировать текущую кассу?'
      : 'Вы действительно хотите активировать следующие кассы?'),
  },
  {
    text: 'Заблокировать',
    key: 'blockKkt',
    popupText: ({ singleAction }) => (singleAction
      ? 'Вы действительно хотите заблокировать текущую кассу?'
      : 'Вы действительно хотите заблокировать следующие кассы?'),
  },
  {
    text: 'Разблокировать',
    key: 'unblockKkt',
    popupText: ({ singleAction }) => (singleAction
      ? 'Вы действительно хотите разблокировать текущую кассу?'
      : 'Вы действительно хотите разблокировать следующие кассы?'),
  },
  {
    text: 'Изменить статус',
    key: 'changeKktStatus',
    popupText: ({ singleAction }) => (singleAction
      ? 'Вы действительно хотите изменить статус у текущей кассы?'
      : 'Вы действительно хотите изменить статус у следующих касс?'),
  },
  {
    text: 'Выбор тарифа',
    key: 'changeKktTplan',
    popupText: ({ singleAction }) => (singleAction
      ? 'Вы действительно хотите изменить тариф у текущей кассы?'
      : 'Вы действительно хотите изменить тариф у следующих касс?'),
  },
  {
    text: 'Удалить',
    key: 'delKkt',
    popupText: ({ singleAction }) => (singleAction
      ? 'Вы действительно хотите удалить текущую кассу?'
      : 'Вы действительно хотите удалить следующие кассы?'),
  },
];

const clientActions = [
  {
    text: 'Восстановить пароль',
    key: 'forgotPass',
    popupText: ({ singleAction }) => (singleAction
      ? 'Отправить сообщение для восстановления пароля?'
      : 'Отправить сообщение для восстановления пароля?'),
  },
  {
    text: 'Запросить SMS CTN',
    key: 'retryCtn',
    popupText: ({ singleAction }) => (singleAction
      ? 'Запросить SMS CTN?'
      : 'Запросить SMS CTN?'),
  },
  {
    text: 'Загрузить список ККТ',
    key: 'kktPack',
    popupText: 'Пакетные операции с ККТ',
    disabled: true,
  },
  {
    text: 'Расторгнуть договор',
    key: 'changeKktStatus',
    popupText: ({ singleAction }) => (singleAction
      ? 'Вы действительно хотите расторгнуть договор у данного клиента?'
      : 'Вы действительно хотите расторгнуть договор у данных клиентов?'),
    disabled: true,
  },
];

const vkRequestActions = [
  {
    text: 'Повторный вызов',
    key: 'retryVkRequest',
    popupText: ({ singleAction }) => (singleAction
      ? 'Выполнить повторный вызов?'
      : 'Выполнить повторный вызов у текущих запросов?'),
  },
  {
    text: 'Повторный вызов всех запросов',
    key: 'retryAllVkRequest',
    popupText: () => 'Выполнить повторный вызов всех запросов?',
    hideList: true,
  },
];

const marketCodeActions = [
  {
    text: 'Удалить',
    key: 'deleteMarketCode',
    popupText: () => 'Вы действительно хотите удалить текущий маркет-код?',
  },
];

const fdActions = [
  {
    text: 'Технический разбор',
    key: 'getDocumentTech',
    popupText: () => null,
  },
];

const lessorActions = [
  {
    text: 'Восстановить пароль',
    key: 'forgotPass',
    popupText: ({ singleAction }) => (singleAction
      ? 'Отправить сообщение для восстановления пароля?'
      : 'Отправить сообщение для восстановления пароля?'),
  },
  {
    text: 'Запросить SMS CTN',
    key: 'retryCtn',
    popupText: ({ singleAction }) => (singleAction
      ? 'Запросить SMS CTN?'
      : 'Запросить SMS CTN?'),
  },
  {
    text: 'Загрузить список ККТ',
    key: 'kktPack',
    popupText: 'Пакетные операции с ККТ',
    disabled: true,
  },
  {
    text: 'Расторгнуть договор',
    key: 'changeKktStatus',
    popupText: ({ singleAction }) => (singleAction
      ? 'Вы действительно хотите расторгнуть договор у данного клиента?'
      : 'Вы действительно хотите расторгнуть договор у данных клиентов?'),
    disabled: true,
  },
];

const tplanActions = [
  {
    text: 'Удалить',
    key: 'deleteTplan',
    popupText: () => 'Вы действительно хотите удалить текущий тарифный план?',
  },
];

export {
  useAction,
  kktActions,
  clientActions,
  vkRequestActions,
  marketCodeActions,
  fdActions,
  lessorKktActions,
  lessorActions,
  tplanActions,
};
