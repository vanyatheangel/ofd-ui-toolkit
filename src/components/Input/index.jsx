import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { DateTime } from 'luxon';
import { useField, useFormikContext } from 'formik';
import ReactDatePicker from 'react-datepicker';
import { InputWrapper, isWidthSmall } from './InputWrapper';
import search from '../../assets/images/search.svg';

import { registerLocale } from  'react-datepicker';
import ru from 'date-fns/locale/ru';
registerLocale('ru', ru);

const Title = styled.span`
  font-size: 16px;
  line-height: 1.47;
  color: ${(props) => props.color || props.theme.gray};
  margin-bottom: 10px;
`;

const ErrorMessage = styled.span`
  color: ${(props) => props?.theme?.red};
  display: block;
  margin-top: -28px;
  padding-bottom: 9px;
`;

const StyledInput = styled.input`
  width: ${({ width, small }) => width || isWidthSmall(small)};
  border-radius: 3px;
  box-shadow: inset 0 3px 0 0 rgba(0, 0, 0, 0.13);
  border: solid 1px rgba(170, 170, 170, 0.45);
  background-color: ${(props) => props.theme.white};
  padding: 10px 10px;
  box-sizing: border-box;
  font-size: 17px;
  line-height: 1.47;
  color: ${({ disabled, theme: { dark, gray } }) => (disabled ? gray : dark)};
  /* margin-bottom: ${({ marginBottom }) => marginBottom || '30px'}; */
  cursor: ${(props) => (props.disabled ? 'not-allowed' : 'text')};
  padding-right: ${({ searchIcon }) => searchIcon ? '40px' : '10px'};
  ::placeholder {
    font-size: ${({ placeholderFontSize }) => placeholderFontSize || '17px'};
  }
  font-family: inherit;
`;

const SearchContainer = styled.div`
  position: relative;
  margin-bottom: ${({ marginBottom }) => marginBottom || '30px'};
`;

const SearchIcon = styled.img`
  position: absolute;
  top: 50%;
  right: 10px;
  transform: translateY(-50%);
  cursor: pointer;
`;

const StyledTextArea = styled.textarea`
  width: ${({ width, small }) => width || isWidthSmall(small)};
  border-radius: 3px;
  box-shadow: inset 0 3px 0 0 rgba(0, 0, 0, 0.13);
  border: solid 1px rgba(170, 170, 170, 0.45);
  background-color: ${(props) => props.theme.white};
  padding: 10px 10px;
  box-sizing: border-box;
  font-size: 17px;
  line-height: 1.47;
  color: ${({ disabled, theme: { dark, gray } }) => (disabled ? gray : dark)};
  /* margin-bottom: ${({ marginBottom }) => marginBottom || '30px'}; */
  cursor: ${(props) => (props.disabled ? 'not-allowed' : 'text')};
  overflow: hidden;
  resize: none;
  font-family: inherit;
`;

const Tip = styled.span`
  font-family: inherit;
  margin-bottom: 25px;
  font-size: 13px;
  color: ${(props) => props.theme.gray};
  line-height: 1.31;
`;

const Input = React.forwardRef(({
  title,
  type,
  placeholder,
  onChange,
  onFocus,
  onBlur,
  defaultValue,
  disabled,
  textArea,
  readOnly,
  error,
  afterFilters,
  exceptionAfterFilters,
  width,
  id,
  small,
  tip,
  noMarginRight,
  datepickerClassname,
  searchIcon,
  onSearchClick,
  onPressEnter,
  ...props
}, ref) => {
  const Component = textArea ? StyledTextArea : StyledInput;
  const [ globalValue, setGlobalValue ] = useState(defaultValue);

  useEffect(() => {
    if (defaultValue) {
      if (type === 'date') {
        setGlobalValue(setDefaultDate(defaultValue))
      } else {
        setGlobalValue(defaultValue);
      }
    } else if (props.value) {
      if (type === 'date') {
        setGlobalValue(setDefaultDate(props.value))
      } else {
        setGlobalValue(props.value)
      }
    }
  }, []);

  const handleFilter = (value) => {
    const filters = [...afterFilters];
    if (exceptionAfterFilters) {
      filters.push(...exceptionAfterFilters);
    }
    const filteredValue = filters.reduce((acc, filter) => {
      acc = filter(acc);
      return acc;
    }, value);

    return filteredValue;
  };

  const setDefaultDate = (date) => {
    const dt = DateTime.fromISO(date);
    return dt.toFormat('yyyy-LL-dd');
  }

  const handleChange = (e, isOnBlur) => {
    const inputValue = isOnBlur ? e.target.value.trim() : e.target.value;
    let value = inputValue;
    if (afterFilters) {
      value = handleFilter(inputValue);
    }
    setGlobalValue(value);
    e.target.value = value;

    if (onChange) onChange(e);
  };

  const setDisplayedValue = (value) => {
    if (type === 'date') {
      const dt = DateTime.fromISO(value);
      return dt.toFormat('dd.LL.yyyy');
    }
    return value;
  };

  const handleKeyPress = (e) => {
    if (onPressEnter) {
      if (e.key === 'Enter') {
        onPressEnter(globalValue);
      }
    }
  }

  if (type === 'date') {
    const { setFieldValue } = useFormikContext();
    const [field] = useField(props);
    const { value, ...propsWithoutValue } = props;
    const { value: fieldValue, ...fieldWithoutValue } = field;
    return (
      <InputWrapper width={width} small={small} noMarginRight={noMarginRight}>
        {
          title && (
            <Title>{title}</Title>
          )
        }
        {readOnly
          ? <Title color="black">{setDisplayedValue(defaultValue || ' ')}</Title>   // In the ' ' there is empty symbol (Alt+255)
          : (
            <ReactDatePicker
              {...fieldWithoutValue}
              {...propsWithoutValue}
              selected={(field.value && new Date(field.value)) || null}
              onChange={(val) => {
                setFieldValue(field.name, val);
              }}
              dateFormat="dd.MM.yyyy"
              placeholderText={placeholder}
              className={datepickerClassname}
            />
          )}
        {tip && <Tip className={`tip ${props.name ? `'tip-'${props.name}` : ''}`}>{tip}</Tip> }
        {error && (
          <ErrorMessage style={{ marginTop: '0px' }} className={`error ${props.name ? `error-${props.name}` : ''}`}>{error}</ErrorMessage>
        )}
      </InputWrapper>
    );
  }

  return (
    <InputWrapper width={width} small={small} noMarginRight={noMarginRight}>
      {
        title && (
          <Title>{title}</Title>
        )
      }
      {readOnly
        ? <Title color="black">{setDisplayedValue(defaultValue || ' ')}</Title>   // In the ' ' there is empty symbol (Alt+255)
        : (
          <SearchContainer {...props} searchIcon={searchIcon}>
            <Component
              {...props}
              id={id}
              type={type}
              placeholder={placeholder}
              onChange={handleChange}
              onFocus={onFocus}
              onBlur={(e) => {
                handleChange(e, true);
                return onBlur(e);
              }}
              defaultValue={type === 'date' ? setDefaultDate(defaultValue) : defaultValue}
              value={globalValue || ''}
              disabled={disabled}
              width={width}
              small={small}
              ref={ref}
              searchIcon={searchIcon}
              onKeyPress={handleKeyPress}
            />
            {
              searchIcon
                ? (
                  <SearchIcon src={search} onClick={() => onSearchClick(globalValue)} />
                )
                : null
            }
          </SearchContainer>
        )}
      {tip && <Tip className={`tip ${props.name ? `'tip-'${props.name}` : ''}`}>{tip}</Tip> }
      {error && (
        <ErrorMessage className={`error ${props.name ? `error-${props.name}` : ''}`}>{error}</ErrorMessage>
      )}
    </InputWrapper>
  );
});

Input.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  defaultValue: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ]),
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  error: PropTypes.string,
  id: PropTypes.string,
  placeholderFontSize: PropTypes.string,
};

Input.defaultProps = {
  title: null,
  type: 'text',
  placeholder: null,
  defaultValue: null,
  disabled: false,
  onChange: () => null,
  onFocus: () => null,
  onBlur: () => null,
  error: null,
  id: null,
  placeholderFontSize: null,
};

export default Input;
