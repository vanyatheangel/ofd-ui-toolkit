import styled from 'styled-components';

const Arrow = styled.button`
  background: none;
  border: none;
  color: ${(props) => props.theme.gray};
  padding: 2px 3px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  cursor: pointer;
`;

export default Arrow;
