import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useTable, useFlexLayout } from 'react-table';
import Circle from '../Circle';
import MoreButton from '../MoreButton';
import Checkbox from '../Checkbox';
import { SORT_ASC } from '../../hooks/useFilters';

const Styles = styled.div`
  .table {
    display: flex;
    flex-direction: column;
    overflow: hidden;

    border-spacing: 0;

    font-size: 0.8rem;

    .thead {
      overflow-x: hidden;
    }

    .tbody {
      overflow-x: hidden;
      padding-bottom: ${(props) => `${props.paddingValue}px` || '0'};
    }

    .tr {
     :last-child {
       .td {
         border-bottom: 0;
       }
     }
    }

    .th,
    .td {
      margin: 0;
      padding: 20px;
      border-bottom: 1px solid #e6e6e6;
      text-align: left;
      position: relative;


      :last-child {
        border-right: 0;
      }
    }

    .th {
      font-weight: bold;
    }
  }
`;

const getStyles = (props, align = 'left') => [
  props,
  {
    style: {
      justifyContent: align === 'right' ? 'flex-end' : 'flex-start',
      alignItems: 'flex-start',
      display: 'flex',
      cursor: 'pointer',
    },
  },
];

const headerProps = (props) => getStyles(props);

const cellProps = (props) => getStyles(props);

const Table = ({
  columns, data, changeSortField, sortField, sortDirection, onRowClick, onRowMouseDown, onRowMouseUp, disablePointerCursor,
}) => {
  const defaultColumn = React.useMemo(
    () => ({
      // When using the useFlexLayout:
      minWidth: 30, // minWidth is only used as a limit for resizing
      width: 150, // width is used for both the flex-basis and flex-grow
      maxWidth: 200, // maxWidth is only used as a limit for resizing
    }),
    [],
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data,
    defaultColumn,
  },
  useFlexLayout);

  return (
    <div {...getTableProps()} className="table">
      <div>
        {headerGroups.map((headerGroup) => (
          <div
            {...headerGroup.getHeaderGroupProps()}
            className="tr"
          >
            {headerGroup.headers.map((column) => (
              <div
                {...column.getHeaderProps(headerProps)}
                className="th"
                onClick={() => {
                  if (column.id !== 'select') {
                    changeSortField(column.id);
                  }
                }}
              >
                {column.render('Header')}
                {column.id === sortField
                && (sortDirection === SORT_ASC
                  ? '▲'
                  : '▼')}
              </div>
            ))}
          </div>
        ))}
      </div>
      <div {...getTableBodyProps()} className="tbody">
        {rows.map(
          (row) => {
            prepareRow(row);
            return (
              <div
                {...row.getRowProps()}
                className="tr"
              >
                {row.cells.map((cell) => (
                  <div
                    {...cell.getCellProps(cellProps(cellProps))}
                    className="td"
                    onClick={() => {
                      if (cell?.column?.id !== 'select' && cell?.column?.id !== 'singleSelect') {
                        onRowClick(row);
                      }
                    }}
                    onMouseDown={() => {
                      if (cell?.column?.id !== 'select' && cell?.column?.id !== 'singleSelect') {
                        onRowMouseDown(row);
                      }
                    }}
                    onMouseUp={() => {
                      if (cell?.column?.id !== 'select' && cell?.column?.id !== 'singleSelect') {
                        onRowMouseUp(row);
                      }
                    }}
                    style={{
                      ...cell.getCellProps(cellProps(cellProps)).style,
                      cursor: disablePointerCursor ? 'auto' : 'pointer',
                    }}
                  >
                    {cell.render('Cell')}
                  </div>
                ))}
              </div>
            );
          },
        )}
      </div>
    </div>
  );
};

const TableWithStyles = ({
  columns,
  data,
  multiSelect,
  singleSelect,
  kktStatus,
  changeAll,
  changeSingle,
  idList,
  allCheckbox,
  singleItemSelectActions,
  singleItemActionToggle,
  setLastAction,
  setSingleId,
  setAllItemData,
  changeSortField,
  sortField,
  sortDirection,
  fnsStatus,
  onRowClick,
  onRowMouseDown,
  onRowMouseUp,
  disablePointerCursor,
  filterActionItemsFunc,
}) => {
  const columnsCopy = [...columns];
  const [ paddingValue, setPaddingValue ] = useState(null);
  const onMenuMount = (data) => {
    setPaddingValue(data.clientHeight);
  }

  if (multiSelect) {
    columnsCopy.unshift({
      Header: () => (
        <Checkbox
          checked={allCheckbox}
          onChange={changeAll}
        />
      ),
      id: 'select',
      // eslint-disable-next-line react/prop-types
      Cell: ({ row: { original: { id } } }) => (
        <Checkbox
          onChange={() => changeSingle(id)}
          checked={idList[id]}
        />
      ),
      width: 30,
      minWidth: 30,
    });
  }

  if (kktStatus) {
    columnsCopy.push({
      Header: () => null,
      id: 'kktStatus',
      // eslint-disable-next-line react/prop-types
      Cell: ({ row: { original: { astatus } } }) => {
        let color;
        const greenStatuses = ['A'];
        const redStatuses = [ 'B', 'N', 'F' ];

        if (greenStatuses.includes(astatus)) {
          color = 'green';
        } else if (redStatuses.includes(astatus)) {
          color = 'red';
        } else {
          color = 'gray';
        }
        return <Circle color={color} />;
      },
      width: 30,
      minWidth: 30,
    });
  }

  if (fnsStatus) {
    columnsCopy.push({
      Header: () => 'ФНС',
      id: 'fnsStatus',
      // eslint-disable-next-line react/prop-types
      Cell: ({ row: { original: { fns_status } } }) => (
        <Circle color={fns_status === 'E' ? 'red' : 'green'} />
      ),
      width: 30,
      minWidth: 30,
    });
  }

  if (singleSelect) {
    columnsCopy.push({
      Header: () => null,
      id: 'singleSelect',
      Cell: ({ row }) => (
        <MoreButton
          rowData={row}
          dropdownItems={singleItemSelectActions}
          singleItemActionToggle={singleItemActionToggle}
          setSingleId={() => setSingleId(row?.original?.id || row?.original?.kktId)}
          setLastAction={setLastAction}
          setAllItemData={setAllItemData}
          onMenuMount={onMenuMount}
          filterActionItemsFunc={filterActionItemsFunc}
        />
      ),
      width: 50,
      minWidth: 50,
    });
  }
  return (
    <Styles paddingValue={paddingValue}>
      <Table
        columns={columnsCopy}
        data={data}
        changeSortField={changeSortField}
        sortField={sortField}
        sortDirection={sortDirection}
        onRowClick={onRowClick}
        onRowMouseDown={onRowMouseDown}
        onRowMouseUp={onRowMouseUp}
        disablePointerCursor={disablePointerCursor}
      />
    </Styles>
  );
};

TableWithStyles.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  multiSelect: PropTypes.bool,
  singleSelect: PropTypes.bool,
  selectActions: PropTypes.arrayOf(PropTypes.shape({})),
  kktStatus: PropTypes.bool,
  changeAll: PropTypes.func,
  changeSingle: PropTypes.func,
  onRowClick: PropTypes.func,
  onRowMouseDown: PropTypes.func,
  onRowUp: PropTypes.func,
  allCheckbox: PropTypes.bool,
  idList: PropTypes.shape({}),
};

TableWithStyles.defaultProps = {
  multiSelect: false,
  singleSelect: false,
  selectActions: [],
  kktStatus: false,
  changeAll: () => {},
  changeSingle: () => {},
  onRowClick: () => {},
  onRowMouseDown: () => {},
  onRowMouseUp: () => {},
  allCheckbox: false,
  idList: {},
};

export default TableWithStyles;
