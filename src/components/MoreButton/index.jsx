import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';
import Circle from '../Circle';

const MoreButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  cursor: pointer;
  padding: 2px 4px;
  position: relative;
`;

const Dropdown = styled.ul`
  background: ${(props) => props.theme.white};
  // display: ${(props) => (props.opened ? 'block' : 'none')};
  display: flex;
  visibility: ${(props) => (props.opened ? 'visible' : 'hidden')};
  flex-direction: column;
  justify-content: space-around;
  z-index: 1;
  padding: 0 17px;
  list-style: none;
  min-width: 160px;
  border-radius: 2px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.24);
  cursor: default;
  position: absolute;
  right: 0;
`;

const DropdownItem = styled.li`
  white-space: nowrap;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  height: 30px;
  margin: 12px 0;
  font-size: 17px;
  line-height: 1.47;
  text-align: right;
  color: ${(props) => (props.disabled ? props.theme.lightGray : props.theme.dark)};
  cursor: ${(props) => (props.disabled ? 'not-allowed' : 'pointer')};
`;

const CircleWithProps = () => <Circle size="5px" extendStyle="margin-top: 2px; background-color: #d8d8d8" />;

const MoreButton = ({
  rowData, dropdownItems, singleItemActionToggle, setLastAction, setSingleId, setAllItemData = () => {}, onMenuMount, filterActionItemsFunc,
}) => {
  const [ isDropdownOpened, setIsOpened ] = useState(false);
  const dropdownRef = useRef(null);
  const dropdownMenuRef = useRef(null);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!dropdownRef.current.contains(event.target)) {
        setIsOpened(false);
      }
    };

    onMenuMount(dropdownMenuRef.current);

    window.addEventListener('click', handleClickOutside);
    return () => {
      window.removeEventListener('click', handleClickOutside);
    };
  }, []);

  return (
    <MoreButtonWrapper onClick={() => setIsOpened(true)} ref={dropdownRef}>
      <CircleWithProps />
      <CircleWithProps />
      <CircleWithProps />
      {dropdownItems?.length && (
        <Dropdown opened={isDropdownOpened} ref={dropdownMenuRef}>
          {dropdownItems.filter((item, index) => filterActionItemsFunc ? filterActionItemsFunc(rowData, item, index) : item).map(({ disabled, text, customOnClick }) => (
            <DropdownItem
              key={text}
              onClick={() => {
                if (customOnClick) {
                  customOnClick(rowData);
                } else {
                  setLastAction(text);
                  setSingleId();
                  singleItemActionToggle();
                  setAllItemData(rowData);
                }
              }}
              disabled={disabled}
            >
              {text}
            </DropdownItem>
          ))}
        </Dropdown>
      )}
    </MoreButtonWrapper>
  );
};

export default MoreButton;
