import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: block;
  position: relative;
`;

const Text = styled.span`
  display: inline-block;
  background: ${(props) => props.theme.lightGray};
  position: absolute;
  font-size: 13px;
  padding: 12px 17px;
  border-radius: 4px;

  &::before {
    content: '';
    position: absolute;
    border: 12px solid transparent;
    ${(props) => {
      const arrowPosition = props.arrowPosition;
      if (arrowPosition === 'top') return 'top';
      if (arrowPosition === 'bottom') return 'bottom';
      if (arrowPosition === 'left') return 'left';
      if (arrowPosition === 'right') return 'right';
    }}: -20px;
    border-${(props) => {
      const arrowPosition = props.arrowPosition;
      if (arrowPosition === 'top') return 'bottom';
      if (arrowPosition === 'bottom') return 'top';
      if (arrowPosition === 'left') return 'right';
      if (arrowPosition === 'right') return 'left';
    }}: 10px solid ${(props) => props.theme.lightGray};
  }
`;

export const Tooltip = ({
  children,
  text,
  isActive,
  arrowPosition,
  textStyle,
  wrapperStyle,
  childrenWrapperStyle
}) => {
  const [ isShown, setIsShown ] = useState(false);

  return (
    <>
      {
        isActive
          ? (
            <Wrapper style={wrapperStyle}>
              <div
                style={{ display: 'inline-block', ...childrenWrapperStyle }}
                onMouseOver={() => setIsShown(true)}
                onMouseLeave={() => setIsShown(false)}
              >
                {children}
              </div>
              {isShown && (
                <Text style={textStyle} arrowPosition={arrowPosition}>
                  {text}
                </Text>
              )}
            </Wrapper>
          )
          : children
      }
    </>
  );
};

Tooltip.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.string,
  ]).isRequired,
  text: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.string,
  ]).isRequired,
  isActive: PropTypes.bool,
  arrowPosition: PropTypes.string,
  textStyle: PropTypes.shape({}),
  wrapperStyle: PropTypes.shape({}),
  childrenWrapperStyle: PropTypes.shape({}),
};

Tooltip.defaultProps = {
  isActive: true,
  arrowPosition: 'top',
  textStyle: {top: '26px', left: '0'},
  wrapperStyle: {},
  childrenWrapperStyle: {},
};

export default Tooltip;
