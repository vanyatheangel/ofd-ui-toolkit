import { useState } from 'react';

export const SORT_ASC = 'asc';
export const SORT_DESC = 'desc';

const useFilters = (arrayOfValues = [], defaultSortField = 'show_id', defaultSortDirection = SORT_ASC) => {
  const [ sortField, setSortField ] = useState(defaultSortField);
  const [ sortDirection, setSortDirection ] = useState(defaultSortDirection);

  const filters = {
    requestParams: {
      startRow: 0,
      endRow: 30,
      sort: [{
        colId: sortField,
        sort: sortDirection,
      }],
      filter: {},
    },
  };

  const changeSortField = (newField) => {
    if (sortField !== newField) {
      setSortField(newField);
      setSortDirection(SORT_ASC);
    } else {
      // flip sort direction values
      setSortDirection(sortDirection === SORT_ASC
        ? SORT_DESC
        : SORT_ASC);
    }
  };

  arrayOfValues.forEach(({ field, value, type = 'eq', isOrCondition = 0, filterType = 'text' }) => {
    if (field && value) {
      filters.requestParams.filter = {
        ...filters.requestParams.filter,
        [field]: {
          filter: value,
          type,
          isOrCondition,
          filterType,
        },
      };
    }
  });

  return {
    filters,
    changeSortField,
    sortField,
    sortDirection,
  };
};

export default useFilters;
