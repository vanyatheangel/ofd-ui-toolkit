import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ButtonWrapper = styled.div`
  position: relative;
  display: inline-block;
  max-width: 100%;
  user-select: none;

  ${(props) => props.extendWrapperStyle}
`;

const StyledButton = styled.button`
  width: ${({ large }) => large ? '231px' : 'auto'};
  position: relative;
  display: inline-block;
  height: ${(props) => props.theme.baseButtonHeight};
  margin-bottom: 4px;
  padding: 0 20px;
  cursor: pointer;
  text-align: center;
  vertical-align: middle;
  white-space: nowrap;
  text-decoration: none;
  background: ${({ background, theme }) => (background ? theme[background] : theme.yellow)};
  color: ${(props) => props.theme.black};
  border: ${(props) => (props.background === 'white' ? `1px solid ${props.theme.black}` : 'none')};
  border-radius: 4px;
  font: inherit;
  outline: none;

  &:not(:disabled) {
    transition: 0.1s;

    &:active {
      box-shadow: inset 0px -1px 22px -2px rgba(0, 0, 0, 0.21);
    }
  }

  &:disabled {
    cursor: not-allowed;
    background: ${(props) => (props.background !== 'white' && props.theme.paleYellow)};
    border: ${(props) => (props.background === 'white' ? `1px solid ${props.theme.lightGray}` : 'none')};
  }

  ${(props) => props.extendStyle}
`;

const Text = styled.span`
  position: relative;
  vertical-align: middle;
`;

const Button = ({
  children,
  onClick,
  type,
  disabled,
  color,
  extendStyle,
  extendWrapperStyle,
  large,
  ...props
}) => (
  <ButtonWrapper extendWrapperStyle={extendWrapperStyle}>
    <StyledButton
      onClick={onClick}
      disabled={disabled}
      type={type}
      background={color}
      extendStyle={extendStyle}
      large={large}
      {...props}
    >
      <Text>
        {children}
      </Text>
    </StyledButton>
  </ButtonWrapper>
);

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.string,
  ]),
  onClick: PropTypes.func,
  type: PropTypes.oneOf([ 'submit', 'button', 'reset' ]),
  disabled: PropTypes.bool,
  color: PropTypes.oneOf([ 'white', 'yellow' ]),
  extendStyle: PropTypes.string,
  extendWrapperStyle: PropTypes.string,
};

Button.defaultProps = {
  children: 'Кнопка',
  type: 'button',
  onClick: () => {},
  disabled: false,
  color: 'yellow',
  extendStyle: null,
  extendWrapperStyle: null,
};

export default Button;
